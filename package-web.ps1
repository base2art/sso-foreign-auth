



$base = Resolve-Path "~"
$children = Get-ChildItem "$($base)\.nuget\packages\base2art.webapirunner.commandlineinterface" |
        Sort-Object -Descending { [version]($_.Name -replace '^[^\d]+-(.*\d)-.*', '$1') }


$baseItem = $children[0]
. "$($baseItem.FullName)/tools/install.ps1"
$baseItem = $children[0]
echo $baseItem
. "$($baseItem.FullName)/content/deploy.ps1"



$deploymentProcessor = New-Webapi-Deployment

$deploymentProcessor.bin("src\Base2art.SSO.ForeignAuth\bin\Release\netstandard2.0")


$deploymentProcessor.config("conf.yaml", "Prod", "configuration.yaml")


echo $deploymentProcessor.deploy()


Exit 0
