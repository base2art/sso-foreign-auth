import http.server
import socketserver
import urllib.request

PORT = 2001

#class Proxy(http.server.SimpleHTTPRequestHandler):
#    def do_GET(self):
#        self.copyfile(urllib.urlopen(self.path), self.wfile)

cache = {}


class MyRequestHandler(http.server.SimpleHTTPRequestHandler):
    def do_GET(self):
        if self.path == "favicon.ico" or self.path == "/favicon.ico":
            self.send_error(404,'File Not Found: %s' % self.path)
            return;
        

        if (len(self.path) > 1):
            data = ""

            #print(cache)

            if self.path in cache:
                data = cache[self.path]
            else:

                url = "https://bitbucket.org/base2art/riotjs-template/raw/master/src" + self.path;
                print(url)
                
                try:
                    response = urllib.request.urlopen(url)
                    data = response.read()      # a `bytes` object
                    
                    try: data = data.encode()
                    except: pass
                except:
                    response = urllib.request.urlopen(url)
                    data = response.read()      # a `bytes` object
                    try: data = data.encode()
                    except: pass
                cache[self.path] = data
            
            self.send_response(200)
            if (self.path.endswith(".js")):
                self.send_header("Content-type", "application/javascript")
            if (self.path.endswith(".html")):
                self.send_header("Content-type", "text/html")
            if (self.path.endswith(".css")):
                self.send_header("Content-type", "text/css")

            content = cache[self.path]
            self.send_header("Content-length", len(data))
            self.end_headers()
            self.wfile.write(content)
        else:
            #if self.path == '/':
            #    self.path = '/simplehttpwebpage_content.html'
            return http.server.SimpleHTTPRequestHandler.do_GET(self)

Handler = MyRequestHandler

#Handler = http.server.SimpleHTTPRequestHandler


#def do_GET(self):
#    self.send_response(200)
#    self.send_header("Content-type", "text/html")
#    self.send_header("Content-length", len(DUMMY_RESPONSE))
#    self.end_headers()
#    self.wfile.write(DUMMY_RESPONSE)

with socketserver.TCPServer(("", PORT), Handler) as httpd:
    print("serving at port", PORT)
    httpd.serve_forever()