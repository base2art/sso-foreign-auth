﻿namespace Base2art.SSO.ForeignAuth
{
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public interface IAuthProvider
    {
        string Name { get; }

        Task<string> SignIn(Dictionary<string, string> data);
    }
}