namespace Base2art.SSO.ForeignAuth.Google
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using global::Google.Apis.Auth;

    public class GoogleAuthProvider : IAuthProvider
    {
        public string Name { get; } = "Google";

        public async Task<string> SignIn(Dictionary<string, string> data)
        {
            var validationSettings = new GoogleJsonWebSignature.ValidationSettings();

            var token = await GoogleJsonWebSignature.ValidateAsync(data["token"], validationSettings);

            return token.Subject;
        }
    }
}