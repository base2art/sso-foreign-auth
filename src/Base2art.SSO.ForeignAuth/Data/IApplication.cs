namespace Base2art.SSO.ForeignAuth.Data
{
    public interface IApplication
    {
        string[] KeyNames { get; }
        string Domain { get; }
    }
}