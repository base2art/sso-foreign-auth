namespace Base2art.SSO.ForeignAuth.Data
{
    using System;

    public interface IKey
    {
        Guid Name { get; }
        bool IsActive { get; }
        DateTime Activation { get; }
        IKeyData KeyData { get; }
    }
}