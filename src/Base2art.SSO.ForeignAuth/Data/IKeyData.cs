namespace Base2art.SSO.ForeignAuth.Data
{
    public interface IKeyData
    { 
        
        byte[] D { get; }
        byte[] DP { get; }
        byte[] DQ { get; }
        byte[] Exponent { get; }
        byte[] InverseQ { get; }
        byte[] Modulus { get; }
        byte[] P { get; }
        byte[] Q { get; }
    }
}