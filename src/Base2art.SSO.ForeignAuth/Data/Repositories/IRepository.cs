namespace Base2art.SSO.ForeignAuth.Data.Repositories
{
    using System;
    using System.Threading.Tasks;

    public interface IRepository
    {
        Task AddKey(Guid name);

//        Task<IApplication> GetApplicationByDomain(string domain);

        Task<IKey> GetKeyByName(Guid keyName);

        Task<IKey> GetMostRecentKey();
        
        Task<IKey[]> GetAll();
    }
}