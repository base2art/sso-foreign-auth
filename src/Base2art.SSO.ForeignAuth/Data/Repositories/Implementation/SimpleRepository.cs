namespace Base2art.SSO.ForeignAuth.Data.Repositories.Implementation
{
    using System;
    using System.Linq;
    using System.Security.Cryptography;
    using System.Security.Cryptography.X509Certificates;
    using System.Threading.Tasks;
    using dbo;
    using DataStorage;
    using IDataStore = DataStorage.IDataStore;

    public class SimpleRepository : IRepository
    {
        private readonly IDataStore dataStore;

        public SimpleRepository(IDataStoreFactory dataStoreFactory)
        {
            this.dataStore = dataStoreFactory.Create("primary");
        }

//        public Task<IApplication> GetApplicationByDomain(string domain)
//        {
//            var application = new Application();
//            application.Domain = domain;
//
//            return new Task<IApplication>(application);
//        }

        public Task AddKey(Guid name)
        {
            RSACryptoServiceProvider myRSA = new RSACryptoServiceProvider(2048);

            var key = myRSA.ExportParameters(true);

            return this.dataStore.Insert<key_parameters>()
                       .Record((rs) => rs.Field(x => x.d, key.D)
                                         .Field(x => x.exponent, key.Exponent)
                                         .Field(x => x.modulus, key.Modulus)
                                         .Field(x => x.p, key.P)
                                         .Field(x => x.q, key.Q)
                                         .Field(x => x.dp, key.DP)
                                         .Field(x => x.dq, key.DQ)
                                         .Field(x => x.inverseq, key.InverseQ)
                                         .Field(x => x.activation_date, DateTime.UtcNow)
                                         .Field(x => x.is_active, true)
                                         .Field(x => x.name, name))
                       .Execute();
        }

        public async Task<IKey> GetKeyByName(Guid keyName)
        {
            var row = await this.dataStore.SelectSingle<key_parameters>()
                                .Where((rs) => rs.Field(x => x.name, keyName, (x, y) => x == y)
                                                 .Field(x => x.is_active, true, (x, y) => x == y))
                                .Execute();

            return this.KeyMap(row);
        }

        public async Task<IKey[]> GetAll()
        {
            var row = await this.dataStore.Select<key_parameters>()
                                .Where(rs => rs.Field(x => x.is_active, true, (x, y) => x == y))
                                .OrderBy(rs => rs.Field(x => x.activation_date, ListSortDirection.Descending))
                                .Execute();

            return row.Select(this.KeyMap).ToArray();
        }

        public async Task<IKey> GetMostRecentKey()
        {
            var row = await this.dataStore.SelectSingle<key_parameters>()
                                .Where(rs => rs.Field(x => x.is_active, true, (x, y) => x == y))
                                .OrderBy(rs => rs.Field(x => x.activation_date, ListSortDirection.Descending))
                                .Execute();

            return this.KeyMap(row);
        }

        private IKey KeyMap(key_parameters row)
        {
            var key = new Key
                      {
                          KeyData = this.Convert(row),
                          Name = row.name,
                          Activation = row.activation_date,
                          IsActive = row.is_active
                      };
            return key;
        }

        private class Key : IKey
        {
            public Guid Name { get; set; }
            public bool IsActive { get; set; }
            public DateTime Activation { get; set; }
            public IKeyData KeyData { get; set; }
        }

//        private class Application : IApplication
//        {
//            public string[] KeyNames { get; } = new[] {"fd2c974f-1e04-4ac1-a934-53f6d18bdaee"};
//
//            public string Domain { get; set; }
//        }

        public ApplicationKeyData Convert(key_parameters parms)
        {
            return new ApplicationKeyData
                   {
                       D = parms.d,
                       DP = parms.dp,
                       DQ = parms.dq,
                       Exponent = parms.exponent,
                       InverseQ = parms.inverseq,
                       Modulus = parms.modulus,
                       P = parms.p,
                       Q = parms.q,
                   };
        }

        public class ApplicationKeyData : IKeyData
        {
            public byte[] D { get; set; }
            public byte[] DP { get; set; }
            public byte[] DQ { get; set; }
            public byte[] Exponent { get; set; }
            public byte[] InverseQ { get; set; }
            public byte[] Modulus { get; set; }
            public byte[] P { get; set; }
            public byte[] Q { get; set; }
        }
    }
}