namespace Base2art.SSO.ForeignAuth
{
    using System;
    using System.IO;
    using System.Runtime.CompilerServices;
    using System.Security.Cryptography;

    public static class KeyExporting
    {
        public static string ExportPublicKey(this RSACryptoServiceProvider csp)
            => WriteForKey(csp.ExportPublicKey);

        public static string ExportPublicKey(this RSAParameters parameters)
            => WriteForKey((x) => ExportPublicKey(parameters, x));

        public static void ExportPublicKey(this RSACryptoServiceProvider csp, TextWriter outputStream)
            => ExportPublicKey(csp.ExportParameters(false), outputStream);

        public static void ExportPublicKey(this RSAParameters rsaParameters, TextWriter outputStream)
            => WriteFor((x) => WritePublic(x, rsaParameters), "PUBLIC", outputStream);

        public static string ExportPrivateKey(this RSACryptoServiceProvider csp)
            => WriteForKey(csp.ExportPrivateKey);

        public static string ExportPrivateKey(this RSAParameters parameters)
            => WriteForKey((x) => parameters.ExportPrivateKey(x));

        public static void ExportPrivateKey(this RSACryptoServiceProvider csp, TextWriter outputStream)
        {
            if (csp.PublicOnly)
            {
                throw new ArgumentException("CSP does not contain a private key", nameof(csp));
            }

            var rsaParameters = csp.ExportParameters(true);

            ExportPrivateKey(rsaParameters, outputStream);
        }

        public static void ExportPrivateKey(this RSAParameters rsaParameters, TextWriter outputStream)
            => WriteFor((x) => WritePrivate(x, rsaParameters), "RSA PRIVATE", outputStream);

        private static void WritePublic(BinaryWriter innerWriter, RSAParameters rsaParameters)
        {
            innerWriter.Write((byte) 0x30); // SEQUENCE
            innerWriter.EncodeLength(13);
            innerWriter.Write((byte) 0x06); // OBJECT IDENTIFIER
            var rsaEncryptionOid = new byte[]
                                   {
                                       0x2a,
                                       0x86,
                                       0x48,
                                       0x86,
                                       0xf7,
                                       0x0d,
                                       0x01,
                                       0x01,
                                       0x01
                                   };

            innerWriter.EncodeLength(rsaEncryptionOid.Length);
            innerWriter.Write(rsaEncryptionOid);
            innerWriter.Write((byte) 0x05); // NULL
            innerWriter.EncodeLength(0);
            innerWriter.Write((byte) 0x03); // BIT STRING
            using (var bitStringStream = new MemoryStream())
            {
                var bitStringWriter = new BinaryWriter(bitStringStream);
                bitStringWriter.Write((byte) 0x00); // # of unused bits
                bitStringWriter.Write((byte) 0x30); // SEQUENCE
                using (var paramsStream = new MemoryStream())
                {
                    var paramsWriter = new BinaryWriter(paramsStream);
                    paramsWriter.EncodeIntegerBigEndian(rsaParameters.Modulus); // Modulus
                    paramsWriter.EncodeIntegerBigEndian(rsaParameters.Exponent); // Exponent
                    var paramsLength = (int) paramsStream.Length;
                    bitStringWriter.EncodeLength(paramsLength);
                    bitStringWriter.Write(paramsStream.GetBuffer(), 0, paramsLength);
                }

                var bitStringLength = (int) bitStringStream.Length;
                innerWriter.EncodeLength(bitStringLength);
                innerWriter.Write(bitStringStream.GetBuffer(), 0, bitStringLength);
            }
        }

        private static void WritePrivate(BinaryWriter innerWriter, RSAParameters rsaParameters)
        {
            innerWriter.EncodeIntegerBigEndian(new byte[] {0x00}); // Version
            innerWriter.EncodeIntegerBigEndian(rsaParameters.Modulus);
            innerWriter.EncodeIntegerBigEndian(rsaParameters.Exponent);
            innerWriter.EncodeIntegerBigEndian(rsaParameters.D);
            innerWriter.EncodeIntegerBigEndian(rsaParameters.P);
            innerWriter.EncodeIntegerBigEndian(rsaParameters.Q);
            innerWriter.EncodeIntegerBigEndian(rsaParameters.DP);
            innerWriter.EncodeIntegerBigEndian(rsaParameters.DQ);
            innerWriter.EncodeIntegerBigEndian(rsaParameters.InverseQ);
        }

        private static void WriteFor(Action<BinaryWriter> newFunction, string name, TextWriter outputStream)
        {
            using (var stream = new MemoryStream())
            {
                var writer = new BinaryWriter(stream);
                writer.Write((byte) 0x30); // SEQUENCE
                using (var innerStream = new MemoryStream())
                {
                    var innerWriter = new BinaryWriter(innerStream);
                    newFunction(innerWriter);

                    var length = (int) innerStream.Length;
                    writer.EncodeLength(length);
                    writer.Write(innerStream.GetBuffer(), 0, length);
                }

                WriteOutput(outputStream, stream, name);
            }
        }

        private static string WriteForKey(Action<TextWriter> action)
        {
            using (var writer = new StringWriter())
            {
                action(writer);
                writer.Flush();
                return writer.GetStringBuilder().ToString();
            }
        }

        private static void WriteOutput(TextWriter outputStream, MemoryStream stream, string name)
        {
            var base64 = Convert.ToBase64String(stream.GetBuffer(), 0, (int) stream.Length).ToCharArray();

            outputStream.WriteLine($"-----BEGIN {name} KEY-----");
            // Output as Base64 with lines chopped at 64 characters
            for (var i = 0; i < base64.Length; i += 64)
            {
                outputStream.WriteLine(base64, i, Math.Min(64, base64.Length - i));
            }

            outputStream.WriteLine($"-----END {name} KEY-----");
        }
    }
}