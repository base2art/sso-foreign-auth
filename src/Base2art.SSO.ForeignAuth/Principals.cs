namespace Base2art.SSO.ForeignAuth
{
    using System;
    using System.Security.Claims;
    using System.Security.Principal;

    public class PrincipalService : IPrincipalService
    {
        private readonly bool skipCheck;

        public PrincipalService(bool skipCheck)
        {
            this.skipCheck = skipCheck;
        }

        public void RequireAuthenticated(IPrincipal principal)
        {
            if (this.skipCheck)
            {
                return;
            }

            var isAuthenticated = principal?.Identity?.IsAuthenticated;

            if (!isAuthenticated.GetValueOrDefault())
            {
                throw new NotAuthenticatedException();
            }
        }

        public string GetUserName(ClaimsPrincipal principal)
        {
            return principal?.Identity?.Name ??
                   (this.skipCheck ? "Local Development" : "(Unknown)");
        }
    }

    public interface IPrincipalService
    {
        void RequireAuthenticated(IPrincipal principal);

        string GetUserName(ClaimsPrincipal principal);
    }

    public class NotAuthenticatedException : Exception
    {
    }
}