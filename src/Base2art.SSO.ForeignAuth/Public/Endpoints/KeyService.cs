namespace Base2art.SSO.ForeignAuth.Public.Endpoints
{
    using System;
    using System.Linq;
    using System.Security.Claims;
    using System.Threading.Tasks;
    using Data;
    using Data.Repositories;
    using Mappings;
    using Resources;

    public class KeyService
    {
        private readonly IRepository keys;

        public KeyService(IRepository keys)
        {
            this.keys = keys;
        }

        public async Task<PublicApplicationKey> Get(Guid id)
        {
            var key = await this.keys.GetKeyByName(id);
            if (key?.KeyData == null)
            {
                return null;
            }

            return key.Map();
        }

        public async Task<PublicApplicationKey[]> GetAllKeys(ClaimsPrincipal principal)
        {
            var keys = await this.keys.GetAll();
            return keys.Select(x => x.MapAdvanced()).ToArray();
        }

        public async Task<PublicApplicationKey> CreateKey(ClaimsPrincipal principal)
        {
            var name = Guid.NewGuid();

            await this.keys.AddKey(name);

            return await this.Get(name);
        }
    }
}