namespace Base2art.SSO.ForeignAuth.Public.Endpoints
{
    using System;
    using System.Collections.Generic;
    using System.IdentityModel.Tokens.Jwt;
    using System.Linq;
    using System.Security.Claims;
    using System.Security.Cryptography;
    using System.Threading.Tasks;
    using Data;
    using Data.Repositories;
    using Microsoft.IdentityModel.Tokens;
    using Resources;

    public class SessionService
    {
        private readonly IAuthProvider[] authProviders;
        private readonly IRepository keys;
        private readonly SecuritySettings settings;

        public SessionService(
            IAuthProvider[] authProviders,
            IRepository keys,
            SecuritySettings settings)
        {
            this.authProviders = authProviders;
            this.keys = keys ?? throw new ArgumentNullException(nameof(keys));
            this.settings = settings;
        }

        public Task<Session> GetSession(ClaimsPrincipal principal)
        {
            var session = new Session();
            session.Status = principal.Identity.IsAuthenticated ? SignInStatus.Success : SignInStatus.UnknownUser;

            return Task.FromResult(session);
        }

        public Task<Session> DeleteSession()
        {
            return Task.FromResult(new Session
                                   {
                                       Name = null,
                                       Token = null,
                                       Status = SignInStatus.UnknownUser
                                   });
        }

        public async Task<Session> AddSession(string authType, Dictionary<string, string> data)
        {
            var authprovider = this.authProviders.FirstOrDefault(x => string.Equals(x.Name, authType, StringComparison.OrdinalIgnoreCase));

            if (authprovider == null)
            {
                throw new ArgumentOutOfRangeException(nameof(authType), "Unsupported AuthType");
            }

            var tokenName = await authprovider.SignIn(data);
            
            if (string.IsNullOrWhiteSpace(tokenName))
            {
                throw new ArgumentNullException(nameof(tokenName), "Authentication name not found.");
            }
            
            var session = new Session();

            var sessionName = $"{authType.ToUpperInvariant()}:{tokenName}";
            var tokenObj = await this.CreateToken(sessionName);
            var tokenObjString = await this.CreateToken(tokenObj);
            var payload = tokenObjString.Split('.')[1];

            session.Name = sessionName;
            session.Status = SignInStatus.Success;
            session.Token = tokenObjString;
            session.RawToken = Base64UrlEncoder.Decode(payload);
            session.Expires = tokenObj.ValidTo;

            return session;
        }

        private Task<string> CreateToken(JwtSecurityToken token)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            return Task.FromResult(tokenHandler.WriteToken(token));
        }

        private async Task<JwtSecurityToken> CreateToken(string name)
        {
            var claimsList = new List<Claim>
                             {
                                 new Claim(JwtRegisteredClaimNames.Sub, name),
                             };

            var appKey = await this.keys.GetMostRecentKey();

            var end = DateTime.UtcNow.Add(this.settings.AdditionalDuration);

            JwtSecurityToken jwtToken = new JwtSecurityToken(
                                                             this.settings.Issuer,
                                                             this.settings.Audience,
                                                             claimsList,
                                                             DateTime.UtcNow.AddMinutes(-10),
                                                             end,
                                                             this.BuildCredentials(appKey));
            return jwtToken;
        }

        private SigningCredentials BuildCredentials(IKey keyData)
        {
            var x = keyData.KeyData;
            RSACryptoServiceProvider myRSA = new RSACryptoServiceProvider();
            myRSA.ImportParameters(new RSAParameters
                                   {
                                       D = x.D,
                                       DP = x.DP,
                                       DQ = x.DQ,
                                       Exponent = x.Exponent,
                                       InverseQ = x.InverseQ,
                                       Modulus = x.Modulus,
                                       P = x.P,
                                       Q = x.Q,
                                   });

            var key = new RsaSecurityKey(myRSA);
            key.KeyId = keyData.Name.ToString("N");
            return new SigningCredentials(key, SecurityAlgorithms.RsaSha256);
        }
    }
}