namespace Base2art.SSO.ForeignAuth.Public.Filters
{
    using System;
    using System.Collections.Generic;
    using System.Net;
    using Resources;
    using Web.Filters.Specialized;

    public class SetCookieFilter : SetCookieFilter<Session>
    {
        private readonly SecuritySettings settings;

        public SetCookieFilter(SecuritySettings settings) => this.settings = settings;

        protected override void SetCookies(Session content, IList<Cookie> cookies)
        {
            if (content == null)
            {
                return;
            }

            if (string.IsNullOrWhiteSpace(content.Token) || string.IsNullOrWhiteSpace(content.RawToken))
            {
                return;
            }

            var expires = DateTime.UtcNow.Add(this.settings.CookieLength);

            if (string.IsNullOrWhiteSpace(this.settings.CookieDomain))
            {
                cookies.Add(new Cookie("auth_info", content.RawToken, "/")
                            {
                                Secure = false,
                                Expires = expires
                            });

                cookies.Add(new Cookie("auth_jwt", content.Token, "/")
                            {
                                Secure = this.settings.RequireSecureCookie,
                                Expires = expires
                            });
            }
            else
            {
                cookies.Add(new Cookie("auth_info", content.RawToken, "/", this.settings.CookieDomain)
                            {
                                Secure = false,
                                Expires = expires
                            });

                cookies.Add(new Cookie("auth_jwt", content.Token, "/", this.settings.CookieDomain)
                            {
                                Secure = this.settings.RequireSecureCookie,
                                Expires = expires
                            });
            }
        }
    }
}