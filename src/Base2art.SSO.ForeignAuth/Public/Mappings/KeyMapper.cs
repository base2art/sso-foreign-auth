namespace Base2art.SSO.ForeignAuth.Public.Mappings
{
    using System.Security.Cryptography;
    using Data;
    using Resources;

    public static class KeyMapper
    {
        public static PublicApplicationKey Map(this IKey key)
        {
            var publicKey = new PublicApplicationKey();
            publicKey.Modulus = key.KeyData.Modulus;
            publicKey.Exponent = key.KeyData.Exponent;
            publicKey.Id = key.Name;
            publicKey.IsActive = key.IsActive;
            publicKey.Activation = key.Activation;

            publicKey.KeyText = new RSAParameters()
                                {
                                    Modulus = publicKey.Modulus,
                                    Exponent = publicKey.Exponent,
                                }.ExportPublicKey();

            return publicKey;
        }
        
        public static PublicApplicationKey MapAdvanced(this IKey key)
        {
            var publicKey = new PublicApplicationKey();
            publicKey.Modulus = key.KeyData.Modulus;
            publicKey.Exponent = key.KeyData.Exponent;
            publicKey.Id = key.Name;
            publicKey.IsActive = key.IsActive;
            publicKey.Activation = key.Activation;

            publicKey.KeyText = new RSAParameters()
                                {
                                    Modulus = publicKey.Modulus,
                                    Exponent = publicKey.Exponent,
                                }.ExportPublicKey();
            return publicKey;
        }
    }
}