//namespace Poc.CiCd.Web.Public.MessageHandlers
//{
//    using System;
//    using System.Collections.Generic;
//    using System.Linq;
//    using System.Threading.Tasks;
//    using Base2art.MessageQueue;
//    using Collections;
//    using Converters;
//    using Messages;
//    using Models;
//    using Repositories;
//
//    public class BuildProjectBranchMessageHandler : MessageHandlerBase<BuildProjectBranchMessage>
//    {
//        private readonly IBuildService buildService;
//        private readonly IJobStoreService jobStoreService;
//        private readonly IProjectService projectService;
//        private readonly IPublisherService publishingService;
//        private readonly IServerReadRepository servers;
//        private readonly ISourceControlService sourceControlService;
//        private readonly IVersionService versions;
//
//        public BuildProjectBranchMessageHandler(
//            IProjectService projectService,
//            ISourceControlService sourceControlService,
//            IJobStoreService jobStoreService,
//            IBuildService buildService,
//            IPublisherService publishingService,
//            IServerReadRepository servers,
//            IVersionService versions)
//        {
//            this.projectService = projectService;
//            this.sourceControlService = sourceControlService;
//            this.jobStoreService = jobStoreService;
//            this.buildService = buildService;
//            this.publishingService = publishingService;
//            this.servers = servers;
//            this.versions = versions;
//        }
//
//        protected override async Task HandleMessage(BuildProjectBranchMessage message)
//        {
//            var branches = message.BranchInfo;
//            var projectId = message.ProjectId;
//            var projectName = message.ProjectName;
//            var userName = message.UserName;
////            Console.WriteLine($"Building branch: {string.Join(",", branches.Select(x => x.Value))}");
//
//            var buildMessageStore = await this.jobStoreService.CreateJob(
//                                                                         projectId,
//                                                                         projectName,
//                                                                         userName,
//                                                                         branches);
//
//            var data1 = new Dictionary<string, object>
//                        {
//                            {"projectId", projectId},
//                            {"sourceCode", branches}
//                        };
//
//            await this.jobStoreService.AddMessage(buildMessageStore.Id, projectName, JobPhase.Creation, "Starting Build...", data1);
//
//            var project = await this.projectService.GetProjectById(projectId);
//
//            this.BuildInternal(project, branches, buildMessageStore)
//                .RunAway(ex => this.LogError(ex, buildMessageStore, projectId, projectName, branches));
//        }
//
//        private async Task LogError(
//            Exception e,
//            IJobInfo buildMessageStore,
//            Guid projectId,
//            string projectName,
//            IReadOnlyDictionary<Guid, SourceControlBranchData> branches)
//        {
//            await this.jobStoreService.AddMessage(
//                                                  buildMessageStore.Id,
//                                                  projectName,
//                                                  JobPhase.Completion,
//                                                  "Exception",
//                                                  new Dictionary<string, object>
//                                                  {
//                                                      {"project", projectName},
//                                                      {"branches", branches},
//                                                      {"message", e.Message},
//                                                      {"messageType", e.GetType().FullName},
//                                                      {"stackTrace", e.StackTrace}
//                                                  });
//
//            await this.jobStoreService.SetState(buildMessageStore.Id, JobState.CompletedFail);
//        }
//
//        private async Task BuildInternal(IProject project,
//                                         IReadOnlyDictionary<Guid, SourceControlBranchData> branches,
//                                         IJobInfo buildMessageStore)
//        {
//            var projectName = project.Name;
//            var contentLookup = new Dictionary<Guid, byte[]>();
//            var sourceControlInfoLookup = new Dictionary<Guid, SourceControlBranchData>();
//            var buildInstructionLookup = new Dictionary<Guid, IBuildInstruction[]>();
//            await this.jobStoreService.SetState(buildMessageStore.Id, JobState.Working);
//            try
//            {
//                foreach (var sourceControlInfo in project.SourceControl)
//                {
//                    var branch = branches[sourceControlInfo.Id];
//                    var message = new Dictionary<string, object>
//                                  {
//                                      {"project", project.Name},
//                                      {"branch", branch.Name},
//                                      {"branchVersion", branch.Hash},
//                                      {"sourceControl", sourceControlInfo.SourceControlType}
//                                  };
//
//                    await this.jobStoreService.AddMessage(
//                                                          buildMessageStore.Id,
//                                                          projectName,
//                                                          JobPhase.Checkout,
//                                                          "Checking out...",
//                                                          message);
//
//                    var checkout = await this.sourceControlService.StartCheckout(sourceControlInfo.SourceControlType,
//                                                                                 sourceControlInfo.SourceControlData,
//                                                                                 branch.Name);
//
//                    sourceControlInfoLookup[sourceControlInfo.Id] = branch;
//
//                    while (checkout.State.In(CheckoutState.Pending, CheckoutState.Claimed, CheckoutState.Working))
//                    {
//                        await Task.Delay(TimeSpan.FromSeconds(30));
//                        checkout = await this.sourceControlService.GetCheckoutStatus(sourceControlInfo.SourceControlType, checkout.Id);
//                    }
//
//                    contentLookup[sourceControlInfo.Id] =
//                        await this.sourceControlService.GetSource(sourceControlInfo.SourceControlType, checkout.Id);
//
//                    await this.sourceControlService.RemoveSource(sourceControlInfo.SourceControlType, checkout.Id);
//
//                    buildInstructionLookup[sourceControlInfo.Id] = sourceControlInfo.BuildInstructions;
//                }
//
//                var buildServer = await this.servers.GetBuildServerByFeatures(project.RequiredFeatures);
//
//                if (buildServer == Guid.Empty)
//                {
//                    await this.jobStoreService.AddMessage(
//                                                          buildMessageStore.Id,
//                                                          projectName,
//                                                          JobPhase.Build,
//                                                          "Cannot Build (No Server Found)",
//                                                          new Dictionary<string, object>
//                                                          {
//                                                              {"project", project.Name},
//                                                              {"branches", branches},
//                                                              {"buildServer", buildServer}
//                                                          });
//                    await this.jobStoreService.SetState(buildMessageStore.Id, JobState.CompletedFail);
//                    return;
//                }
//
//                await this.jobStoreService.AddMessage(
//                                                      buildMessageStore.Id,
//                                                      projectName,
//                                                      JobPhase.Build,
//                                                      "Building...",
//                                                      new Dictionary<string, object>
//                                                      {
//                                                          {"project", project.Name},
//                                                          {"branches", branches},
//                                                          {"buildServer", buildServer}
//                                                      });
//
////                string oldVersion = await this.versions.GetLastVersion(project.Id, sourceControlInfoLookup);
//                var newVersion = await this.versions.GetNextVersion(
//                                                                    project.Id,
//                                                                    buildMessageStore.Id,
//                                                                    project.Versioning.MapToConcrete(),
//                                                                    sourceControlInfoLookup);
//
//                var build = await this.buildService.StartBuild(
//                                                               buildServer,
//                                                               newVersion,
//                                                               buildInstructionLookup.ConvertToInput(),
//                                                               contentLookup,
//                                                               sourceControlInfoLookup);
//
//                await this.jobStoreService.AddMessage(
//                                                      buildMessageStore.Id,
//                                                      projectName,
//                                                      JobPhase.Build,
//                                                      "Build Running...",
//                                                      new Dictionary<string, object>
//                                                      {
//                                                          {"project", project.Name},
//                                                          {"branches", branches},
//                                                          {"buildServer", buildServer},
//                                                          {"buildId", build.Id},
//                                                          {"version", newVersion}
//                                                      });
//
//                await this.jobStoreService.SetBuildInfo(buildMessageStore.Id, build.ServerId, build.Id);
//
//                while (build.State.In(BuildState.Pending, BuildState.Claimed, BuildState.Working))
//                {
//                    await Task.Delay(TimeSpan.FromSeconds(30));
//                    build = await this.buildService.GetBuildStatus(build.ServerId,
//                                                                   build.Id);
//                }
//
//                if (build.State == BuildState.CompletedFail)
//                {
//                    await this.jobStoreService.AddMessage(
//                                                          buildMessageStore.Id,
//                                                          projectName,
//                                                          JobPhase.Completion,
//                                                          "Exception",
//                                                          new Dictionary<string, object>
//                                                          {
//                                                              {"project", project.Name},
//                                                              {"branches", branches},
//                                                              {"buildState", build.State.ToString("G")}
//                                                          });
//
//                    await this.jobStoreService.SetState(buildMessageStore.Id, JobState.CompletedFail);
//                    return;
//                }
//
//                var artifacts = await this.buildService.GetArtifacts(
//                                                                     buildServer,
//                                                                     build.Id,
//                                                                     project.SourceControl.Select(x => x.Id).ToArray());
//
//                var artifactNames = artifacts.SelectMany(y => y.Value.Select(x => x.RelativeName)).ToArray();
//
//                await this.jobStoreService.AddMessage(
//                                                      buildMessageStore.Id,
//                                                      projectName,
//                                                      JobPhase.Build,
//                                                      "Build Completed...",
//                                                      new Dictionary<string, object>
//                                                      {
//                                                          {"project", project.Name},
//                                                          {"branches", branches},
//                                                          {"buildServer", buildServer},
//                                                          {"buildId", build.Id},
//                                                          {"artifacts", artifactNames}
//                                                      });
//
////                await this.buildService.RemoveBuild(buildServer, build.Id);
//
//                foreach (var artifact in artifacts)
//                {
//                    await this.publishingService.Publish(
//                                                         project.Id,
//                                                         buildMessageStore.Id,
//                                                         sourceControlInfoLookup,
//                                                         artifact.Value.Select(FileDataConverter.ConvertToInput).ToArray());
//                }
//
//                await this.jobStoreService.SetState(buildMessageStore.Id, JobState.CompletedSuccess);
//
//                await this.jobStoreService.AddMessage(
//                                                      buildMessageStore.Id,
//                                                      projectName,
//                                                      JobPhase.Completion,
//                                                      "Success",
//                                                      new Dictionary<string, object>
//                                                      {
//                                                          {"project", project.Name},
//                                                          {"branches", branches},
//                                                          {"version", newVersion},
//                                                          {"artifacts", artifacts.SelectMany(x => x.Value.Select(y => y.RelativeName)).ToArray()}
//                                                      });
//            }
//            catch (Exception e)
//            {
//                await this.jobStoreService.AddMessage(
//                                                      buildMessageStore.Id,
//                                                      projectName,
//                                                      JobPhase.Completion,
//                                                      "Exception",
//                                                      new Dictionary<string, object>
//                                                      {
//                                                          {"project", project.Name},
//                                                          {"branches", branches},
//                                                          {"message", e.Message},
//                                                          {"messageType", e.GetType().FullName},
//                                                          {"stackTrace", e.StackTrace}
//                                                      });
//
//                await this.jobStoreService.SetState(buildMessageStore.Id, JobState.CompletedFail);
//                return;
//            }
//        }
//    }
//}
//
////        public async Task<JobInfo> Build(IProject project, IReadOnlyDictionary<Guid, IBranchData> branches)
////        {
////        
////        Console.WriteLine($"Building branch: {string.Join(",", branches.Select(x => x.Value.Name))}");
////
////        var newBranches = branches.ToDictionary(
////                                                x => x.Key,
////                                                x => x.Value.ConvertToInput());
////        var buildMessageStore = await this.jobStoreService.CreateJob(project.Id,
////                                                                     newBranches);
////
////            this.BuildInternal(project, branches, buildMessageStore)
////            .RunAway(ex => this.LogError(ex, buildMessageStore, project, branches));
////
////            return buildMessageStore.MapToConcrete();
////        }