namespace Base2art.SSO.ForeignAuth.Public.Resources
{
    using System;

    public class PublicApplicationKey
    {
        public Guid Id { get; set; }
        public byte[] Modulus { get; set; }
        public byte[] Exponent { get; set; }
        public DateTime Activation { get; set; }
        public bool IsActive { get; set; }
        public string KeyText { get; set; }
    }
}