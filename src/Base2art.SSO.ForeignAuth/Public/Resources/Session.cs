namespace Base2art.SSO.ForeignAuth.Public.Resources
{
    using System;

    public class Session
    {
        public string Name { get; set; }

        public string Token { get; set; }

        public string RawToken { get; set; }

        public SignInStatus Status { get; set; }
        
        public DateTime Expires { get; set; }
    }
}