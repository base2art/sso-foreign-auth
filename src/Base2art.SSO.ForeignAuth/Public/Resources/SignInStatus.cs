namespace Base2art.SSO.ForeignAuth.Public.Resources
{
    public enum SignInStatus
    {
        Success, 
        LockedOut, 
        BadPassword, 
        UnknownUser, 
        InvalidRequest
    }
}