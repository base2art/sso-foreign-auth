namespace Base2art.SSO.ForeignAuth.Public.Tasks
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using System.Threading.Tasks;
    using Data.Repositories.Implementation.dbo;
    using DataStorage;

    public class ProvisionDatabaseTask
    {
        private readonly IDbmsFactory factory;

        public ProvisionDatabaseTask(IDbmsFactory factory)
        {
            this.factory = factory;
        }

        public async Task ExecuteAsync()
        {
            try
            {
                await this.factory.Create("primary").CreateTable<key_parameters>().Execute();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }
    }
}