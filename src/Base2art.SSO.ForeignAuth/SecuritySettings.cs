namespace Base2art.SSO.ForeignAuth
{
    using System;
    using System.Net;
    using System.Net.Security;

    public class SecuritySettings
    {
        public SecuritySettings(string cookieDomain, bool requireSecureCookie, string audience, string issuer)
        {
            this.RequireSecureCookie = requireSecureCookie;
            this.CookieDomain = cookieDomain;

            this.Audience = audience;
            this.Issuer = issuer;
            if (cookieDomain == "localhost")
            {
                ServicePointManager.ServerCertificateValidationCallback += (sender, certificate, chain, errors) =>
                {
                    if (sender is HttpWebRequest request && request.Host == "localhost")
                    {
                        return true;
                    }

                    return errors == SslPolicyErrors.None;
                };
            }
        }

        public TimeSpan AdditionalDuration { get; } = TimeSpan.FromDays(1);

        public string Audience { get; }

        public string Issuer { get; }

        public TimeSpan CookieLength => TimeSpan.FromMinutes((this.AdditionalDuration.TotalMinutes * 9) / 10);

        public string CookieDomain { get; }

        public bool RequireSecureCookie { get; }
    }
}