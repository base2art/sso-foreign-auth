namespace Base2art.SSO.ForeignAuth.Features
{
    using System;
    using System.Security.Cryptography;
    using FluentAssertions;
    using Newtonsoft.Json;
    using Xunit;
    using Xunit.Abstractions;

    public class UnitTest1
    {
        private readonly ITestOutputHelper helper;

        public UnitTest1(ITestOutputHelper helper)
        {
            this.helper = helper;
        }

        [Fact]
        public void Test1()
        {
            var data = @"
{
    ""D"": ""PbS66tAf0juQQxjzIV7oTWNRXXPdylq37yHT4zj5pl0I/x9cU9Pr/OS/zwYzgYK6Bj7guBmGzM9x9b749jf5aL43ffbNXFLV0p7RkyyiUBVmdgy36fTw+noDL47s32LX4MTdK6tfHwvssa9d8XWiC+OAlHFyyIvqB0SKbLgJff0="",
    ""DP"": ""PfS/k1Wyy3DoI801WPZSlzDow9Gec4opvro/jqGrYMaFflySzpL6k0Pby9RgxgzUei5LcrJydGge2aMsUIpvtQ=="",
    ""DQ"": ""fHJyqFZ+ofU9GZ82TA5xRad6l45D9yrBYslTUjrAdb40RTQBXVATNYAlZMY6cPPmPrLZnCYdoPgkkeihFkJ/uQ=="",
    ""Exponent"": ""AQAB"",
    ""InverseQ"": ""BnqDwEREWL+/0Lo+hlYJkydgyniBUwHl1XoDSMNjHd/aPNq/DLen/f5F8e6Lg/a19Neg8abjDVBKsIoScWPvwA=="",
    ""Modulus"": ""zDu9BhO2IV+w9ny9C/Wgg0xhFAQ7FjR+d01qvU/y9Znu29E+MG8Umxn7k1JtS4Tp0GiNr0bY/6dy4ujhO3iOBkNW8efZsR3IMcI9XBhoXTAZ9CSapEQJm9/lbK6Wjxfo1xm23m02X3gYtNDpJ6TDtzUQhPjG8mrnXQe/WdyVFU0="",
    ""P"": ""z8dgLmQBy+kYgZpOhgjm8kp1sykVyZCCfNENqWYgrV0rr9rqh/LqQdiipTgnipe7IizFWeq9ntTxljsh+kWkjw=="",
    ""Q"": ""+6G3/UrF51MdRyaC8ryXLKWtSAH8kVGcFTBas18FqLqbBfkqH8tOfXOfXRIYwzusXJOrUnze2fnJzV2SPH9uYw==""
}

";

            var parms = Newtonsoft.Json.JsonConvert.DeserializeObject<RSAParameters>(data);
            var provider = new RSACryptoServiceProvider();
            provider.ImportParameters(parms);

            var publicKey = @"-----BEGIN PUBLIC KEY-----
MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDMO70GE7YhX7D2fL0L9aCDTGEU
BDsWNH53TWq9T/L1me7b0T4wbxSbGfuTUm1LhOnQaI2vRtj/p3Li6OE7eI4GQ1bx
59mxHcgxwj1cGGhdMBn0JJqkRAmb3+VsrpaPF+jXGbbebTZfeBi00OknpMO3NRCE
+MbyauddB79Z3JUVTQIDAQAB
-----END PUBLIC KEY-----
";

            var privateKey = @"-----BEGIN RSA PRIVATE KEY-----
MIICWwIBAAKBgQDMO70GE7YhX7D2fL0L9aCDTGEUBDsWNH53TWq9T/L1me7b0T4w
bxSbGfuTUm1LhOnQaI2vRtj/p3Li6OE7eI4GQ1bx59mxHcgxwj1cGGhdMBn0JJqk
RAmb3+VsrpaPF+jXGbbebTZfeBi00OknpMO3NRCE+MbyauddB79Z3JUVTQIDAQAB
AoGAPbS66tAf0juQQxjzIV7oTWNRXXPdylq37yHT4zj5pl0I/x9cU9Pr/OS/zwYz
gYK6Bj7guBmGzM9x9b749jf5aL43ffbNXFLV0p7RkyyiUBVmdgy36fTw+noDL47s
32LX4MTdK6tfHwvssa9d8XWiC+OAlHFyyIvqB0SKbLgJff0CQQDPx2AuZAHL6RiB
mk6GCObySnWzKRXJkIJ80Q2pZiCtXSuv2uqH8upB2KKlOCeKl7siLMVZ6r2e1PGW
OyH6RaSPAkEA+6G3/UrF51MdRyaC8ryXLKWtSAH8kVGcFTBas18FqLqbBfkqH8tO
fXOfXRIYwzusXJOrUnze2fnJzV2SPH9uYwJAPfS/k1Wyy3DoI801WPZSlzDow9Ge
c4opvro/jqGrYMaFflySzpL6k0Pby9RgxgzUei5LcrJydGge2aMsUIpvtQJAfHJy
qFZ+ofU9GZ82TA5xRad6l45D9yrBYslTUjrAdb40RTQBXVATNYAlZMY6cPPmPrLZ
nCYdoPgkkeihFkJ/uQJABnqDwEREWL+/0Lo+hlYJkydgyniBUwHl1XoDSMNjHd/a
PNq/DLen/f5F8e6Lg/a19Neg8abjDVBKsIoScWPvwA==
-----END RSA PRIVATE KEY-----
";
            provider.ExportPrivateKey().Should().Be(privateKey);
            provider.ExportPublicKey().Trim().Should().Be(publicKey.Trim());
        }
    }
}