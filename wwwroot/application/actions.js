var ApplicationActions = (function (wndw1) {

    let wndw = wndw1;
    var configure = function (app) {
        app.addAction("authorizeGoogle", function (name, token) {
            app.module("webClient")
                .rest("PUT", '/api/session/Google')
                .withBody({token: token, name: name})
                .do(body => (console.log(body)));
        });


        app.addAction("reload", function () {

            let tag = document.getElementById("myBabaBuiApp")._tag;
            tag.update();
        });

        app.addAction("gotoApplication", function () {

            let canGotoApplication = wndw.location.hostname !== "localhost";
            if (canGotoApplication) {
                const url = new URL(wndw.location);
                url.hostname = url.hostname.replace("sso.", "");
                wndw.location = url;
            } else {
                return "Signed in but cannot navigate";
                // alert("Would navigate if i could...")
            }
        });


    };

    return {
        configure: configure
    };


})(window);
