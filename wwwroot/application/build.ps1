$blankFile = @'
<!doctype html>

<html>

<head>
    <meta charset="UTF-8">
    <title>{title}</title>
    <link rel="shortcut icon" href="application/images/favicon.png">
    <meta http-equiv="Content-Type" content="text/html; charset=us-ascii">
    {application-styles}
    {application-scripts}
    
    <!--[if lt IE 9]>
    <script src="//cdnjs.cloudflare.com/ajax/libs/es5-shim/4.0.5/es5-shim.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
    <![endif]-->
</head>


<body>
<div class="content-wrapper">
    <div id="myBabaBuiApp"></div>
</div>


<!-- Tags-->
{bababui-tags}
{application-tags}

<!-- RIOT-->
{bababui-compiler}

{bababui-scripts}

<!-- Loader -->
<script>

    babaBuiAppFactory.registerModule(babaBuiIO);
    babaBuiAppFactory.registerModule(babaBuiNavigation);
    babaBuiAppFactory.registerModule(babaBuiSecurity);
    babaBuiAppFactory.registerModule(babaBuiNet);
    babaBuiAppFactory.registerModule(babaBuiWebClient);
    let app = babaBuiAppFactory.createApplication();

    ApplicationRoutes.configure(app);
    ApplicationActions.configure(app);

    riot.mount("div#myBabaBuiApp", "application-loader", {app: app});
    app.navigation.registerNavigatedHandler(function(){
        document.title = app.navigation.currentPage.title;
        let element = document.getElementById("myBabaBuiApp");
        if (element && element._tag) {
            element._tag.update();
        }
    });


    if (window.location.hash) {
        var hash = window.location.hash.substring(1); //Puts hash in variable, and removes the # character
        app.navigation.routeToUrl(hash);
    }
    else {
        app.navigation.routeToUrl(app.navigation.routes().filter(x => x.parameters.length === 0)[0].url, {});
    }


</script>


</body>
</html>
'@


function dl($baseUrl, $file)
{
    $base = "https://bitbucket.org/base2art/riotjs-template/raw/master/src"
    $outBase = "$baseUrl/3rd-party/baba-bui"
    $dir = Split-Path "$outBase/$file"

    if (Test-Path "$outBase/$file")
    {
        return;
    }

    if (-Not(Test-Path $dir))
    {
        [system.io.directory]::CreateDirectory($dir)
    }

    Invoke-WebRequest -Uri "$base/$file" -OutFile "$outBase/$file"
}

$bababuiTags = @(
"tags/application-loader.tag.html",
"tags/parts/main-navigation.tag.html",
"tags/parts/is-working.tag.html",
"tags/parts/page-title.tag.html",
"tags/parts/sign-in.tag.html",
"tags/parts/panel-placeholder.tag.html",
"tags/parts/panels-placeholder.tag.html",
"tags/parts/errors.tag.html",
"tags/parts/application-footer.tag.html"
"tags/parts/application-footer.tag.html"
"tags/parts/user-form.tag.html"
"tags/parts/user-form-field.tag.html"
);

$bababuiScripts = @(

"scripts/appFactory.js",
"scripts/io.js",
"scripts/navigation.js",
"scripts/security.js",
"scripts/net.js"
"scripts/webClient.js"
);


$baseUrl = pwd
$baseUrl = Split-Path $baseUrl
echo $baseUrl

$bababuiTagsContent = ""

foreach ($bababuiTag in $bababuiTags)
{
    $bababuiTagsContent += "`n" + "<script src='3rd-party/baba-bui/$bababuiTag' type='riot/tag'></script>"
    dl $baseUrl $bababuiTag
}

$bababuiScriptsContent = ""

foreach ($bababuiScript in $bababuiScripts)
{
    $bababuiScriptsContent += "`n" + "<script src='3rd-party/baba-bui/$bababuiScript'></script>"
    dl $baseUrl $bababuiScript
}


$bababuiCompilerContent = "<script src='3rd-party/riot/riot-compiler.min.js'  charset='UTF-8'></script>"
$bababuiCompilerContent = "<script src='https://cdn.jsdelivr.net/npm/riot@3.13.2/riot+compiler.js'></script>"


$party3s = Get-ChildItem -Path "$baseUrl/3rd-party" | ?{ $_.PSIsContainer }

$appRoot = Get-Item -Path "$baseUrl/application"

$applicationScriptsContent = ""
$applicationStylesContent = ""
$applicationTagsContent = ""

function FindBy($appender, $dir, $template, $extension, $list)
{

    $childDirs = Get-ChildItem -path $dir | ?{ $_.PSIsContainer }

    $childFiles = Get-ChildItem -path $dir -Filter $extension | ?{ -not$_.PSIsContainer }

    foreach ($item in $childDirs)
    {
        $newList = @()
        foreach ($io in $list)
        {
            $newList += $io
        }
        $newList += $item.Name
        $appender = FindBy $appender $item $template $extension $newList
    }

    foreach ($item in $childFiles)
    {
        $appender += $template.replace("{FILE}", "$( $list -Join "/" )/$( $item.Name )")

    }

    return $appender
}

foreach ($party3 in $party3s)
{
    if ($party3.Name -eq "baba-bui" -or $party3.Name -eq "riot")
    {

    }
    else
    {

        #echo $party3.Name

        $applicationStylesContent = FindBy $applicationStylesContent $party3 "`n<link rel='stylesheet' type='text/css' href='3rd-party/$( $party3.Name )/{FILE}'>" "*.css" @()

        $applicationScriptsContent = FindBy $applicationScriptsContent $party3 "`n<script src='3rd-party/$( $party3.Name )/{FILE}'></script>" "*.js"  @()

        #$js = Get-ChildItem -Path "$baseUrl/3rd-party/$($party3.Name)" -Recurse -Filter "*.js"
        #$css = Get-ChildItem -Path "$baseUrl/3rd-party/$($party3.Name)" -Recurse -Filter "*.css"
        #$svg = Get-ChildItem -Path "$baseUrl/3rd-party/$($party3.Name)" -Filter "*.svg"
    }
}


$applicationScriptsContent = FindBy $applicationScriptsContent $appRoot "`n<script src='{FILE}'></script>" "*.js"  @("application")
$applicationStylesContent = FindBy $applicationStylesContent $appRoot "`n<link rel='stylesheet' type='text/css' href='{FILE}'>" "*.css" @("application")
$applicationTagsContent = FindBy $applicationTagsContent $appRoot "`n<script src='{FILE}' type='riot/tag'></script>" "*.tag.html" @("application")

$blankFile = $blankFile.Replace("{title}", "Application");
$blankFile = $blankFile.Replace("{application-styles}", $applicationStylesContent);
$blankFile = $blankFile.Replace("{application-scripts}", $applicationScriptsContent);
$blankFile = $blankFile.Replace("{application-tags}", $applicationTagsContent);
$blankFile = $blankFile.Replace("{bababui-tags}", $bababuiTagsContent);
$blankFile = $blankFile.Replace("{bababui-compiler}", $bababuiCompilerContent);
$blankFile = $blankFile.Replace("{bababui-scripts}", $bababuiScriptsContent);

Set-Content -Path "$baseUrl/index.html" -Value  $blankFile
#<script src='application/tags/application-header.tag.html' type='riot/tag'></script>
