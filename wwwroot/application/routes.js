var ApplicationRoutes = (function () {

    var configure = function (app) {
        
        // let user = {name:null};
        let user = {name:"SjY"};


        app.security.setAuthenticator((function (u) {
            
            let us = u;
            return {
                id: function () {
                    return us.name;
                },
                isAuthenticated: function () {
                    // debugger;
                    return us.name != null && us.name.length > 0;
                },
                hasRole: function (role) {
                    return true;
                },
                canSignIn: function () {
                    return true;
                },
                signIn: function () {

                }
            };
        })(user));
        
        app.navigation.panel("header", "application-header")
            .add();

        app.navigation.panel("notifications", "debug")
            .modelProvider((params, callback) => {
                app.webClient.rest("GET", "/api/session")
                    .do(function(body) {

                        if (body.status === "Success") {
                            user.name = "Success";
                            let result = app.actions.gotoApplication();
                            callback({message: result});
                        }
                    });

                // callback({companyName: "Base2art", companyUrl: "//base2art.com/"});
            })
            .add();


        app.navigation.panel("leftNav", "left-nav").add();
        // app.navigation.panel("leftNav", "left-nav").add();
        app.navigation.panel("footer", "application-footer")
            .modelProvider((params, callback) => {
                callback({companyName: "Base2art", companyUrl: "//base2art.com/"});
            })
            .add();
        

        app.navigation.addRoute("Google Auth", "/auth/google", [])
            .panel("main", "google-auth").add();
        app.navigation.addRoute("Facebook Auth", "/auth/facebook", [])
            .panel("main", "facebook-auth").add();

        app.navigation.addRoute("Keys", "/keys", [])
            .panel("main", "keys-view")//.requireRole("Keys:View")
            .modelProvider((params, callback) => {

                app.webClient.rest("GET", "/api/keys")
                    .do(body => callback({
                        title: "Keys",
                        dataTypes: {
                            "Key": "code",
                            "Created": "date-since"
                        },
                        items: body.map(y => {
                            return {
                                Id: y.id,
                                Created: y.activation,
                                "Is Active": y.isActive,
                                "Key": y.keyText
                            };
                        })
                    }));

            }).add();

        // app.navigation.addRoute("Google Auth", "/google-auth", [])
        //     .panel("main", "page-one").requireRole("Dashboard:View").add();
        //
    };

    return {
        configure: configure
    };


})();
